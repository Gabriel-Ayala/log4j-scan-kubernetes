FROM ubuntu:focal

WORKDIR /home/ubuntu
RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl python3 python3-pip git nano && \
    curl -LO https://dl.k8s.io/release/v1.23.0/bin/linux/amd64/kubectl && \
    chmod +x kubectl  && \
    mv kubectl /usr/bin && \
    curl -LO  https://github.com/aquasecurity/trivy/releases/download/v0.21.2/trivy_0.21.2_Linux-64bit.tar.gz &&\
    tar -zxvf trivy_0.21.2_Linux-64bit.tar.gz &&\
    mv trivy /usr/bin &&\
    pip install awscli &&\
    curl -fsSL https://get.docker.com | bash

COPY log4j_scan.sh /home/ubuntu
ENTRYPOINT ["sh","/home/ubuntu/log4j_scan.sh","CVE-2021-44228"]